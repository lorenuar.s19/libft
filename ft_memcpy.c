/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/13 10:42:32 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/02 11:33:20 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t size)
{
	unsigned char		*pdst;
	unsigned const char	*psrc;

	pdst = (unsigned char*)dst;
	psrc = (unsigned const char*)src;
	if (pdst == psrc)
		return (dst);
	if (!pdst && !psrc)
		return (NULL);
	while (size-- > 0)
		*(pdst++) = *(psrc++);
	return (dst);
}
