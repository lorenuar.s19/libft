/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 16:04:57 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/25 14:50:04 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t size)
{
	unsigned char	*ps1;
	unsigned char	*ps2;

	ps1 = (unsigned char*)s1;
	ps2 = (unsigned char*)s2;
	while (size > 0 && ps1 && ps2 && *ps1 && *ps1 == *ps2)
	{
		ps1++;
		ps2++;
		size--;
	}
	if (size <= 0)
		return (0);
	return (*(unsigned char*)(ps1) - *(unsigned char*)(ps2));
}
