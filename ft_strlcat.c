/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 13:24:31 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/24 16:05:39 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dest, const char *src, size_t size)
{
	size_t	dlen;
	size_t	slen;
	size_t	res;

	dlen = 0;
	while (dest[dlen] != '\0')
		dlen++;
	res = 0;
	while (src[res] != '\0')
		++res;
	if (size <= dlen)
		res += size;
	else
		res += dlen;
	slen = 0;
	while (src[slen] != '\0' && dlen + 1 < size)
	{
		dest[dlen] = src[slen];
		dlen++;
		slen++;
	}
	dest[dlen] = '\0';
	return (res);
}
