/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/29 11:14:13 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/11 12:39:56 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	str_len(const char *s)
{
	size_t		len;

	len = 0;
	while (s && s[len])
		len++;
	return (len);
}

char			*ft_strjoin(const char *s1, const char *s2)
{
	char		*a;
	size_t		l1;
	size_t		l2;

	l1 = str_len(s1);
	l2 = str_len(s2);
	if (!s1 || !s2 || (!(a = ft_calloc(l1 + l2 + 1, sizeof(char)))))
		return (NULL);
	ft_memcpy(a, s1, l1);
	ft_memcpy(&a[l1], s2, l2);
	a[l1 + l2] = '\0';
	return (a);
}
