/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/20 13:32:53 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/25 15:12:46 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	get_num_size(int n)
{
	size_t		len;

	len = 0;
	if (n < 0)
		n = -n;
	while (n > 10 - 1)
	{
		n /= 10;
		len++;
	}
	return (len + 1);
}

char			*ft_itoa(int n)
{
	char			*s;
	ssize_t			si;
	size_t			i;
	size_t			nsize;
	int				neg;

	nsize = get_num_size(n);
	neg = (n < 0) ? 1 : 0;
	i = (n < 0) ? -(long long)n : (long long)n;
	if (n <= INT_MIN)
		return (ft_strdup("-2147483648"));
	si = nsize + neg;
	s = NULL;
	if (!(s = ft_calloc(neg + nsize + 1, sizeof(char))))
		return (s);
	s[si] = '\0';
	while (si-- > neg && (s[si] = i % 10 + '0'))
		i /= 10;
	if (neg)
		s[0] = '-';
	return (s);
}
