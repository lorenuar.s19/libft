/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 11:07:59 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/07 14:35:49 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strtrim(char const *s1, char const *set)
{
	long	i;
	size_t	ofs;
	size_t	len;

	if (!s1 || !set)
		return (NULL);
	i = 0;
	while (s1[i] && ft_strchr(set, s1[i]))
		i++;
	ofs = i;
	i = ft_strlen(s1);
	i = ((i > 0) ? (i - 1) : 0);
	while (s1[i] && ft_strchr(set, s1[i]))
		i--;
	len = 1 + i - ofs;
	if (ofs >= ft_strlen(s1))
		return (ft_calloc(sizeof(char), 1));
	return (ft_substr(s1, ofs, len));
}
