/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 11:45:58 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/24 13:55:29 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s, const char *f, size_t size)
{
	size_t	si;
	size_t	fi;
	int		found;

	si = 0;
	fi = 0;
	if (!*f || s == f)
		return ((char*)s);
	while (s && s[si] && size > 0)
	{
		fi = 0;
		found = 1;
		while (f && f[fi] && found)
		{
			if (s[si + fi] == f[fi])
				found = 1;
			else
				found = 0;
			fi++;
		}
		if (found && si + fi < size)
			return ((char*)&s[si]);
		si++;
	}
	return (NULL);
}
