/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/13 11:05:34 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/02 10:52:11 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t size)
{
	unsigned char		*pdst;
	unsigned const char	*psrc;

	pdst = (unsigned char*)dst;
	psrc = (unsigned const char*)src;
	if (pdst == psrc)
		return (dst);
	if (!pdst && !psrc)
		return (NULL);
	while (pdst < psrc && size-- > 0)
		*(pdst++) = *(psrc++);
	pdst += size;
	psrc += size;
	while (pdst >= psrc && size-- > 0)
		*(--pdst) = *(--psrc);
	return (dst);
}
