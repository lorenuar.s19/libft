/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 09:42:16 by lorenuar          #+#    #+#             */
/*   Updated: 2020/01/16 15:01:43 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcpy(char *dst, const char *src, size_t size)
{
	size_t len;

	len = 0;
	if (!src || !dst || dst == src)
		return (len);
	while (len + 1 < size && *src)
		dst[len++] = *src++;
	if (size)
		dst[len] = '\0';
	return (ft_strlen(src - len));
}
