/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/15 19:55:20 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/27 15:37:23 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strdup(const char *s)
{
	ssize_t	slen;
	ssize_t	i;
	char	*al;

	slen = 0;
	if (s)
		slen = ft_strlen(s);
	if (!(al = ft_calloc(slen + 1, sizeof(char))) && slen && s)
		return (NULL);
	i = -1;
	while (i < slen && s && s[++i])
		al[i] = s[i];
	al[i] = '\0';
	return (al);
}
