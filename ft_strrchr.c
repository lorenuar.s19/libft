/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 15:43:50 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/28 11:09:14 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int		i;

	if (s)
		i = ft_strlen(s);
	while (s && s[i] != (char)c)
	{
		if (i-- <= 0)
			return (NULL);
	}
	return ((char*)s + i);
}
