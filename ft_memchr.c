/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 10:25:09 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/24 15:36:31 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t size)
{
	unsigned char *pstr;

	pstr = (unsigned char*)s;
	while (pstr && size-- > 0)
	{
		if (*pstr == (unsigned char)c)
			return (pstr);
		else
			pstr++;
	}
	return (NULL);
}
