/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/13 16:00:56 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/02 11:27:15 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t size)
{
	unsigned char		*pdst;
	unsigned const char	*psrc;

	pdst = (unsigned char*)dst;
	psrc = (unsigned const char*)src;
	while (pdst && psrc && size-- > 0)
	{
		if ((*pdst = *psrc) == (unsigned char)c)
			return (pdst + 1);
		pdst++;
		psrc++;
	}
	return (NULL);
}
