# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/01/09 11:56:23 by lorenuar          #+#    #+#              #
#    Updated: 2020/02/28 17:04:12 by lorenuar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
#                                                                              #
#	make										compile & link binary          #
#	make re										force recompiling              #
#	make clean									remove objects files           #
#	make fclean									remove binary & object files   #
#                                                                              #
# **************************************************************************** #

# ------------------------------- Variables ---------------------------------- #
# Compiler
CC		= gcc
CFLAGS	= -Wall -Werror -Wextra

# Src files
SRCS	=					\
		ft_atoi.c 			\
		ft_bzero.c			\
		ft_calloc.c			\
		ft_isalnum.c		\
		ft_isalpha.c		\
		ft_isascii.c		\
		ft_isdigit.c		\
		ft_isprint.c		\
		ft_itoa.c			\
		ft_memccpy.c		\
		ft_memchr.c			\
		ft_memcmp.c			\
		ft_memcpy.c			\
		ft_memmove.c		\
		ft_memset.c			\
		ft_putchar_fd.c		\
		ft_putendl_fd.c		\
		ft_putnbr_fd.c		\
		ft_putstr_fd.c		\
		ft_split.c			\
		ft_strchr.c			\
		ft_strdup.c			\
		ft_strjoin.c		\
		ft_strlcat.c		\
		ft_strlcpy.c		\
		ft_strlen.c			\
		ft_strmapi.c		\
		ft_strncmp.c		\
		ft_strnstr.c		\
		ft_strrchr.c		\
		ft_strtrim.c		\
		ft_substr.c			\
		ft_tolower.c		\
		ft_toupper.c		\

B_SRCS	= $(SRCS)			\
		ft_lstadd_back.c	\
		ft_lstadd_front.c	\
		ft_lstclear.c		\
		ft_lstdelone.c		\
		ft_lstiter.c		\
		ft_lstlast.c		\
		ft_lstmap.c			\
		ft_lstnew.c			\
		ft_lstsize.c		\



OBJS	= $(SRCS:.c=.o)
B_OBJS 	= $(B_SRCS:.c=.o)

# LIB Name
NAME	= libft.a

# ---------------------------------- Rules ----------------------------------- #
.PHONY : all re

all : $(NAME)

bonus : $(B_SRCS)
	@echo "\033[32m+ Compiling with flags\033[0m"
	$(CC) $(CFLAGS) -c $(B_SRCS)
	@echo "\033[32m& Linking into $(NAME)\033[0m"
	ar -rcs $(NAME) $(B_OBJS)
	@echo "\033[1;32m= Done\033[0m"

$(NAME) : $(SRCS)
	@echo "\033[32m+ Compiling with flags\033[0m"
	$(CC) $(CFLAGS) -c $(SRCS)
	@echo "\033[32m& Linking into $(NAME)\033[0m"
	ar -rcs $(NAME) $(OBJS)
	@echo "\033[1;32m= Done\033[0m"

clean :
	@echo "\033[31m- Removing \033[1;33mALL \033[0m\033[31mobject files\033[0m"
	rm -f $(B_OBJS)

fclean : clean
	@echo "\033[31m- Removing $(NAME)\033[0m"
	rm -f $(NAME)

re : fclean all

t1 : re bonus
	@-cd ../libft-unit-test && make f
	@make fclean eho
	@echo "\n\n\n= = = RESULT.LOG = = =\n\n\n"
	@cat -n ../libft-unit-test/result.log
	@echo "\n\n\n= = = RESULT.LOG = = =\n\n\n"
	@read -p "Press any key to continue ..."
	@clear

t2 : re bonus
	@-cd ../Libftest && ./grademe.sh
	@make fclean eho
	@echo "\n\n\n= = = RESULT.LOG = = =\n\n\n"
	@cat -n ../libftest/deepthought
	@echo "\n\n\n= = = RESULT.LOG = = =\n\n\n"
	@make eho
	@clear

t3 : re bonus
	@-cd ../libft_tester_agossuin && ./launch.sh
	@make fclean eho
	@clear

t4 : re bonus
	@-cd ../libftdestructor && sh run.sh ../libft 12B
	@make fclean

eho :
	@echo "\033[1;32m= = = Done = = =\033[0m"
	@read -p "Press any key to continue ..."
	@clear
