/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 10:20:12 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/13 12:31:31 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	str_len(const char *s)
{
	size_t		len;

	len = 0;
	while (s && s[len])
		len++;
	return (len);
}

char			*ft_substr(const char *s, unsigned int start, size_t len)
{
	char	*a;
	size_t	slen;

	slen = str_len(s);
	if (!s || start >= slen || len > (slen - start) ||
		(!(a = ft_calloc(len + 1, sizeof(char)))))
		return (NULL);
	ft_memcpy(a, &s[start], len);
	a[len] = '\0';
	return (a);
}
