/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/29 14:24:51 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/24 13:36:46 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	countsep(const char *s, char c)
{
	size_t		csp;

	csp = 0;
	while (*s)
		if (c == *(s++))
			csp++;
	return (((csp != 0) ? (csp + 1) : 0));
}

static char		*split(const char *s, unsigned int start, size_t len)
{
	char	*a;

	if (len == (size_t)-1)
		return ((char *)-1);
	if (!s || (!(a = ft_calloc(len + 1, sizeof(char)))))
		return (NULL);
	ft_memcpy(a, &s[start], len);
	a[len] = '\0';
	return (a);
}

static int		nextsep(const char *s, char c, size_t *si)
{
	size_t	nsep;

	nsep = 0;
	if (c == *s)
	{
		*si += 1;
		return (-1);
	}
	while (s && *s && c != *s++)
		nsep++;
	*si += nsep;
	return (nsep);
}

static	void	free_all(char **a)
{
	while (*a)
		free(*a++);
	free(a);
	return ;
}

char			**ft_split(char const *s, char c)
{
	char		**a;
	size_t		ai;
	size_t		spc;
	size_t		si;

	if (!s || !c)
		return (0);
	spc = countsep(s, c);
	if (!(a = ft_calloc(spc + 1, sizeof(*a))))
		return (NULL);
	ai = 0;
	si = 0;
	while (s && s[si] && ai <= spc)
	{
		a[ai] = split(s, si, (size_t)nextsep(&s[si], c, &si));
		if (!a[ai])
		{
			free_all(a);
			return (NULL);
		}
		if (a[ai] != (char *)-1)
			ai++;
	}
	a[ai] = 0;
	return (a);
}
